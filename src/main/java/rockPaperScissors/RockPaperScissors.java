package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        int userChoise = 0;
        String computerChoiseString = "";

        while (true) {
            int computerChoise = (int)(Math.random() * 3);
            String userChoiseString = "";

            System.out.println("Let's play round " + roundCounter);

            while (true) {
                userChoiseString = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
                if(!(userChoiseString.equals("rock") || userChoiseString.equals("paper") || userChoiseString.equals("scissors"))) {
                    System.out.println("I do not understand " + userChoiseString + ". Could you try again?");
                    continue;
                }
                break;
            }

            if (userChoiseString.equals("rock")) {
                userChoise = 0;
            } else if (userChoiseString.equals("paper")) {
                userChoise = 1;
            } else if (userChoiseString.equals("scissors")) {
                userChoise = 2;
            }

            if (computerChoise == 0) {
                computerChoiseString = "rock";
            } else if (computerChoise == 1) {
                computerChoiseString = "paper";
            } else if (computerChoise == 2) {
                computerChoiseString = "scissors";
            }

            if (userChoise - computerChoise == 0) {
                roundCounter++;
                System.out.println("Human chose " + userChoiseString + ", computer chose " + computerChoiseString + ". It's a tie!\nScore: human " + humanScore + ", computer " + computerScore);
            } else if (userChoise - computerChoise == -1 || userChoise - computerChoise == 2) {
                computerScore++;
                roundCounter++;
                System.out.println("Human chose " + userChoiseString + ", computer chose " + computerChoiseString + ". Computer Wins!\nScore: human " + humanScore + ", computer " + computerScore);
            } else {
                humanScore++;
                roundCounter++;
                System.out.println("Human chose " + userChoiseString + ", computer chose " + computerChoiseString + ". Human Wins!\nScore: human " + humanScore + ", computer " + computerScore);
            }

            String yesOrNo = readInput("Do you wish to continue playing? (y/n)?");
            if (yesOrNo.equals("y")) {
                continue;
            } else if (yesOrNo.equals("n")) {
                break;
            }
        }
        System.out.println("Bye bye :)");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
